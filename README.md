# MQTT Drawer Cabinet Sensor

This project is a small hardware project for me to test using an MQTT broker within the hQ house.

## Table of Contents

- [Description](#Description)
- [Hardware](#Hardware)
  - [Drawers Unit](#Drawers-Unit)
  - [Microcontroller](#Microcontroller)
  - [Switches](#Switches)
  - [Other](#Other)
- [Software](#Software)
  - [Libraries](#Microcontroller-Libraries)
  - [MQTT Broker](#MQTT-Broker)

## Description

The idea behind this project is that I have a set of drawers ([IKEA ALEX 6 drawer cabinet](https://www.ikea.com/se/sv/p/alex-ladhurts-pa-hjul-vit-40196241/)) in the living room which contains many various cables and power supplies (Mostly of the USB kind) and I would like to know when others in hQ are "borrowing" them. I may also for a laugh get Home Assistant to turn the lights red when a drawer is opened or get the TV to play a message through TTS, will depend on how im feeling when I implement it...

## Hardware

### Drawers Unit

This project is designed to be installed on an [IKEA ALEX](https://www.ikea.com/se/sv/p/alex-ladhurts-pa-hjul-gra-50264927/) cabinet as this was what I had already brought when I first moved into hQ.

![IKEA ALEX](https://gitlab.com/hq-smarthome/mqtt-drawers/-/raw/master/docs/IKEA_ALEX_Cabinet.png "IKEA ALEX")

### Microcontroller

This will use a NodeMCU V3 (ESP8266) but feel free to use any other WiFi enabled Microcontroller or Ethernet based one as long as you modify the source code to match that! (As a future plan I want to look into getting one powered via Ethernet PoE as I have several of these cabinets)

![NodeMCU V3 (ESP8266)](https://gitlab.com/hq-smarthome/mqtt-drawers/-/raw/master/docs/NodeMCUv3.jpeg "NodeMCU V3 (ESP8266)")

### Switches

This is currently designed to work with some small microswitches which I brought from [Kjell & Company](https://www.kjell.com/se/) here in Sweden however this will work with any switch that is Push to Make. It would also work with Push to Break but you would need to modify how they connect to the microcontroller. The specific switches used are [Mikrobrytare Extra Liten (p36054)](https://www.kjell.com/se/produkter/el-verktyg/elektronik/elektromekanik/strombrytare/mikrobrytare/mikrobrytare-extra-liten-p36054) as they are just smaller than the height of rails in use of the IKEA ALEX cabinet.

![Small Microswitch](https://gitlab.com/hq-smarthome/mqtt-drawers/-/raw/master/docs/Microswitch.jpeg "Small Microswitch")

### Other

Just to make the installation a bit neater I also got a [Neutrik Panelskarvdon USB3.0 (p67496)](https://www.kjell.com/se/produkter/dator/datortillbehor/usb-tillbehor/usb-kontakter/neutrik-panelskarvdon-usb-3.0-p67496) so that I can pass USB / programming power to the Microcontroller without having to disassemble the unit every time. This means I can pass a USB cable outside ready for connecting. Again brought this from [Kjell & Company](https://www.kjell.com/se/) because it was available right now (USB 3.0 is overkill for this project but I'm too impatient to wait for shipping) and its reversible so that the USB-B connector can be facing outside.

![USB3.0 Panel](https://gitlab.com/hq-smarthome/mqtt-drawers/-/raw/master/docs/USB_Panel.jpeg "USB3.0 Panel")

## Software

I programmed the Microcontroller using [PlatformIO](https://platformio.org/). It was recommended to me from one of my colleagues and I thought this would be a good project to give it a try on. There are a couple of extra libraries that I have added so that I don't have to code most of this project!

### Microcontroller Libraries

#### ESP8266WiFi

For connecting to the WIFi network I am using the [ESP8266WiFi.h](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi) provided by Arduino

#### PubSubClient

For communicating with the MQTT server I have used [PubSubClient.h](https://github.com/knolleary/pubsubclient) provided by Nick O'Leary.

### MQTT Broker

There will be a repo going over how I set up my MQTT broker but I have not made it public yet... so for now have a look up on [Mosquitto MQTT Broker](https://mosquitto.org/) as this is what will be set up; However, feel free to use another broker if you wish.
