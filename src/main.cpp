#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>

WiFiClientSecure espClient;
PubSubClient client(espClient);

char topic[32];
char drawers[6] = {D6, D5, D0, D7, D1, D2};
bool lastReading;
bool drawerValues[6] = {false, false, false, false, false, false};
int numberOfDrawers = sizeof(drawers);
char updateTopic[32];
char startupTopic[32];
char updateAllTopic[16] = "cabinet/update";

#if defined(DEVICE_NAME)
  char deviceName[16] = DEVICE_NAME;
#else
  char deviceName[16] = "drawer";
#endif

void publishSensors(bool force = false) {
  for (int i = 0; i < numberOfDrawers; i = i + 1) {
    lastReading = digitalRead(drawers[i]);

    if (lastReading != drawerValues[i] || force == true) {
      sprintf(topic, "cabinet/%s/drawer/%c", deviceName, i + 48); 

      drawerValues[i] = lastReading;
      client.publish(topic, (lastReading) ? "closed" : "open");
      Serial.print(topic);
      Serial.println((lastReading) ? " closed" : " open");
    }
  }
}

void callback(char* receivedTopic, byte* payload, unsigned int length) {
  Serial.print("Received message: ");
  Serial.println(receivedTopic);

  if (strcmp(receivedTopic, updateTopic) == 0 || strcmp(receivedTopic, updateAllTopic) == 0) {
    publishSensors(true);
  }
}

bool initWiFi() {
  #if defined(SSID_NAME) && defined(SSID_PASSWORD)
    delay(10);

    WiFi.begin(SSID_NAME, SSID_PASSWORD);

    Serial.println("Initiallising WiFi");
    Serial.print("MAC address: ");
    Serial.println(WiFi.macAddress());
    Serial.print("Attempting to connect to ");
    Serial.println(SSID_NAME);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

    randomSeed(micros());

    Serial.println();
    Serial.print("Connected to ");
    Serial.println(SSID_NAME);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    return true;
  #else
    Serial.println("Missing environment variables for WiFi");

    return false;
  #endif
}

void connectMQTT() {
  #if defined(MQTT_USER) && defined(MQTT_PASSWORD)
    while (!client.connected()) {
      Serial.print("Connecting to MQTT broker ");
      Serial.println(MQTT_HOST);

      if (client.connect("ESP8266Client", MQTT_USER, MQTT_PASSWORD)) {
        Serial.println("connected");  
      } else {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(5000);
      }
    }

    sprintf(startupTopic, "cabinet/%s/startup", deviceName); 

    client.publish(startupTopic, "");
  #else
    Serial.println("Missing environment variables for MQTT");

    return false;
  #endif
}

bool initMQTT() {
  #if defined(MQTT_HOST) && defined(MQTT_PORT) && defined(TLS_FINGERPRINT)
    espClient.setFingerprint(TLS_FINGERPRINT);
    client.setServer(MQTT_HOST, MQTT_PORT);
    client.setCallback(callback);

    connectMQTT();

    return true;
  #else
    Serial.println("Missing environment variables for MQTT");

    return false;
  #endif
}

void setup() {
  for (byte i = 0; i < sizeof(drawers); i = i + 1) {
    pinMode(drawers[i], INPUT);
  }

  Serial.begin(115200);

  Serial.println();
  Serial.println("----------------");
  Serial.println("  Draw Monitor  ");
  Serial.println("----------------");
  Serial.println();

  #ifdef PIO_SRC_REV
    Serial.print("Build from rev: ");
    Serial.println(PIO_SRC_REV);
    Serial.println();
  #endif

  sprintf(updateTopic, "cabinet/%s/update", deviceName);

  bool WiFiInitialised = initWiFi();
  bool MQTTInitialised = initMQTT();

  if (WiFiInitialised && MQTTInitialised) {
    client.subscribe(updateTopic);
    client.subscribe(updateAllTopic);
    publishSensors(true);
  } else {
    while (true) {
      Serial.print("Failed to initialise");
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    connectMQTT();
  }

  publishSensors();

  client.loop();
}
